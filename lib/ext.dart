import 'package:flutter/material.dart';
import 'dart:ui' as ui show TextHeightBehavior;


extension ObjectExt on Object {
  void debug(Object obj) {
    assert(() {
      print('==========$runtimeType ========');
      print(obj);
      return true;
    }());
  }

  ///print object self to the console when in the debug mode.
  void console() {
    assert(() {
      print('$runtimeType: $this');
      return true;
    }());
  }

  bool get isNull => (this == null);

  bool get isNotNull => !(this == null);
}

typedef _CurrentWidgetWrapper = Widget Function(Widget currentWidget);

extension WidgetEnableExt on Widget {
  Widget disable() => Container();

  Widget wrapper(_CurrentWidgetWrapper wrapperCB) {
    return wrapperCB(this);
  }

  Widget onlyReplaceAtDebug(Widget replaceWidget) {
    Widget returnWidget;
    returnWidget = this;
    assert(() {
      returnWidget = replaceWidget;
      return true;
    }());

    return returnWidget;
  }
}

extension datetimeExt on DateTime{
  toMinString() {
    return this.hour * 60 + this.minute;
  }
}

extension TextExt on Text{

  /// allow create new widget from old widget and change some parameter
  Text copyWith({
    String data,
    Key key,
    TextStyle style,
    StrutStyle strutStyle,
    TextAlign textAlign,
    TextDirection textDirection,
    Locale locale,
    bool softWrap,
    TextOverflow overflow,
    double textScaleFactor,
    int maxLines,
    String semanticsLabel,
    TextWidthBasis textWidthBasis,
    ui.TextHeightBehavior textHeightBehavior}){
    return Text(
        data ?? this.data,
        key: key ?? this.key,
        style: style ?? this.style,
        strutStyle: strutStyle ?? this.strutStyle,
        textAlign: textAlign ?? this.textAlign,
        textDirection: textDirection ?? this.textDirection,
        locale: locale ?? this.locale,
        softWrap: softWrap ?? this.softWrap,
        overflow: overflow ?? this.overflow,
        textScaleFactor: textScaleFactor ?? this.textScaleFactor,
        maxLines: maxLines ?? this.maxLines,
        semanticsLabel: semanticsLabel ?? this.semanticsLabel,
        textWidthBasis: textWidthBasis ?? this.textWidthBasis,
        textHeightBehavior: textHeightBehavior ?? this.textHeightBehavior
    );
  }

  /// to fixed textScaleFactor to 1.0. witch means the text didn't change by
  /// ios and android system settings
  Text get fixedTextFont => this.copyWith(textScaleFactor: 1.0);
}