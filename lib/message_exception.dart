import 'dart:core';

class MessageException implements Exception {
  final String type;
  final String errorCode;
  final String message;

  const MessageException(this.message, {this.errorCode = '', this.type = ''});

  @override
  String toString() {
    return message;
  }
}
