import 'dart:async';
import 'useful_utility.dart';

typedef OnMissionStart = void Function(String missionName, Map data);

class MissionCtr {
  bool _lock = false;
  List _queue = [];

  final int autoUnlock; //sec
  final OnMissionStart onMissionStart;
  Timer _timer;
  String currentMission;

  MissionCtr(this.onMissionStart, {this.autoUnlock = 10});

  bool get lock {
    return _lock;
  }

  set lock(bool isLock) {
    _lock = isLock;
    if (!_lock) {
      _next();
    }
  }

  missionFinish(String mission) {
    if (currentMission == mission) {
      if (_timer != null) {
        _timer.cancel();
        _timer = null;
      }
      currentMission = null;
      lock = false; //next()會在lock的setter內呼叫
    }
  }

  add(String mission, {Map data = const {}}) {
    _queue.add({"mission": mission, "data": data});
    if (!lock) {
      _next();
    }
  }

  containMission(String mission) {

    debug("containMission: $mission");
    if (currentMission != null && currentMission == mission) {
      return true;
    }

    debug("_queue length:${_queue.length}");
    for (var i = 0; i < _queue.length; i++) {
      Map missionMap = _queue[i];
      if (missionMap["mission"] == mission) {
        return true;
      }
    }

    return false;
  }

  _next() {
    if (lock) {
      return;
    }
    if (_queue.length > 0) {
      if (_timer != null) {
        _timer.cancel();
      }
      _timer = Timer(Duration(seconds: autoUnlock), () {
        debug("時間到沒解lock");
        lock = false;
      });
      lock = true;
      Map missionMap = _queue.removeAt(0);
      currentMission = missionMap["mission"];
      if (onMissionStart != null) {
        onMissionStart(missionMap["mission"], missionMap["data"]);
      }
    }
  }

  clear() {
    _queue.clear();
    if (_timer != null) {
      _timer.cancel();
    }
    lock = false;
  }

  void dispose() {
    clear();
  }
}
