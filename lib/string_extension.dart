import 'package:intl/intl.dart';

extension StringExtension on String {
  String hidePhone({int num = 3, String replace = '***'}) {
    return this.replaceRange(this.length - num, this.length, replace);
  }

  /// to remove the word city, county, District
  String removeLocality() {
    return this.replaceAll(RegExp('(City|County|District)'), '');
  }

  String utcToLocalTimeString() {
    try {
      var timeStr =
          DateFormat("yyyy/MM/dd HH:mm").format(DateTime.parse(this).toLocal());
      return timeStr;
    } catch (e) {
      return "";
    }
  }
}
