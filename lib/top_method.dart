import 'package:flutter/material.dart';

void replaceErrorWidget() {
  bool isDebugMode = false;
  assert(() {
    isDebugMode = true;
    return true;
  }());
  if (isDebugMode) {
    return;
  }
  ErrorWidget.builder = (FlutterErrorDetails flutterErrorDetails) {
    // flutterErrorDetails.toString().console();
    return Material(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(
            Icons.warning,
            color: Colors.grey,
            size: 20.0,
          ),
          SizedBox(
            height: 10.0,
          ),
          Text(
            '出錯了，請聯繫服務人員',
            style: TextStyle(fontSize: 20.0, color: Colors.grey),
          )
        ],
      ),
    );
  };
}